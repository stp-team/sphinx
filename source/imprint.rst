#######
Imprint
#######

This documentation site is part of SystemTestPortal.

SystemTestPortal is a research project of the Institute of Software
Technology at the University of Stuttgart.

**Address:** University of Stuttgart Keplerstrasse 7 70174 Stuttgart Germany

**Telephone:** +49 (0)711/ 685-0

**Fax:** +49 (0)711/ 685-82113

**E-Mail:** poststelle@uni-stuttgart.de

**Internet:** www.uni-stuttgart.de

**Responsible party:** The University of Stuttgart is a public sector
entity. It is legally represented by the Rector, Prof. Dr.-Ing. Wolfram
Ressel. E-Mail

**VAT ID no.:** Pursuant to §27 a Value Added Tax Act: DE 1477 94 196

**External Links:** Links leaving the domain systemtestportal.org are marked as such.
All links outside of the domain systemtestportal.org refer to
externally forwarded contents for which the University of Stuttgart does
not claim any ownership. The responsibility lies with the respective
external provider (see legal note - provider identification). The
external contents were reviewed when setting the link. It cannot be
ruled out that the contents are subsequently changed by the respective
providers. Should you be of the opinion that any of the external
websites to which we are linked infringe upon applicable law or
otherwise have inappropriate contents, please notify us accordingly.
