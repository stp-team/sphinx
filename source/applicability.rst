#############
Applicability
#############

Test cases inside test sequence must have at least one version and variant in common. A test sequence can not be executed otherwise.
An applicability can be created in the test case creation window. It can also be created on the test case editing page.

.. figure:: img/applicability/create_variant_and_version.png
	 :alt: Variant and version creation dialog
	 :align: center

	 Versions And Variants Creation Dialog

The versions and variants creation dialog:
	#. A version is entered here. Clicking on the "Add" button will add it.
 	#. A version is selected here.
 	#. A version is added to a variant here.
 	#. This button saves added or deleted versions and variants.

Afterwards select a variant and click on a version to add them to the test case by clicking on them.

	.. figure:: img/applicability/add_variant_and_version.png
	 	:alt: Variant and version add dialog
	 	:align: center

	 	Adding Versions And Variants
