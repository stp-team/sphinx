#############
Adding A User
#############

New users can be added by clicking on the "Add new user" button when a user is logged in.

The current login informations of the default user are:

    * Name: demo
    * Password: demo

.. figure:: img/add_user/add_new_user_button.png
  :alt: "Add new user" button
  :align: center

  "Add new user" button

This will open a new dialog.

.. figure:: img/add_user/add_new_user.png
  :alt: User creation menu
  :align: center

  User Creation Menu

The user creation menu:
  #. The desired user name is entered here.
  #. The desired display name is entered here.
  #. The email address of the user is entered here.
  #. The desired passwort is entered here.
  #. This button will register the user.
