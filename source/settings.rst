########
Settings
########

The buttons on the left can be used to switch between project settings and permission settings.

.. figure:: img/settings/project_settings_selection_buttons.png
  :alt: settings selection buttons
  :align: center

  Settings Selection Buttons

****************
Project settings
****************

.. figure:: img/settings/Project_Description.png
  :name: Project_Description
  :alt: Project_Description
  :align: center

  Project Settings Page

The project settings page:
  #. A project image can be uploaded here.
  #. The project name is shown here.
  #. The project description is shown here. It can also be changed here.
  #. The project visibility is shown here. It can also be changed here.
  #. Pressing this button saves all changes.
  #. Pressing this button allows the user to export the project.

*******************
Permission settings
*******************

Roles are managed here. Roles are used to customize permissions of different members inside of a project.

.. figure:: img/settings/Permission_Dialog.png
  :name: Permission Settings Page
  :alt: Permission Settings Page
  :align: center

  Permission Settings Page

The permission settings page:
  #. The role name is shown here.
  #. The role permissions are shown here.
  #. The role can be deleted here.
  #. Pressing this button saves all changes.
  #. Pressing this button allows the user to create a new role.

Adding a new role
=================

.. line-block::
  Roles can be added by clicking on the "+Add Role" button in the control bar.
  The button opens a new blank role. This role can be named and the desired permissions can be selected.

.. figure:: img/settings/Permission_Dialog_Adding.png
  :name: Permission_Dialog_Adding
  :alt: Permission_Dialog_Adding

  Permission Dialog Adding

Editing an existing role
========================

Roles can be edited by checking or unchecking the permission boxes.

Deleting an existing role
=========================

Clicking on the "Delete" button starts the deletion process.
Clicking on the "Confirm delete" button deletes the role.
