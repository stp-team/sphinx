#####
TODOs
#####

TODOs show to which test cases/sequences the user is assigned to.
They are created automatically when a user is assigned to a test case/sequence.

They can be viewed by clicking on the TODOs button when a user is logged in.

.. figure:: img/todos/todos_button.png
  :alt: TODOs button
  :align: center

  TODOs button

Clicking on this button will open the "Your todos" page.
There the TODOs can be viewed.
Clicking on "Done" will remove a TODO from the list.

.. figure:: img/todos/todos_page.png
  :alt: TODOs page
  :align: center

  TODOs page
