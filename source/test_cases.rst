##########
Test Cases
##########

********************
Test cases main page
********************

.. figure:: img/test_case/test_case_start_page.png
  :alt: Test cases start page
  :align: center

  Test Cases Main Page

The test cases main page:
  #. The test case search functionality is not implemented yet.
  #. Clicking on this button will open the test case creation page.
  #. A list of all test cases is shown here. Clicking on an entry will direct the user to a more detailed view of the test case.
  #. Labels are managed here. They can also be used to filter the test cases. For more information visit the :doc:`Labels <../labels>` page.

******************
Create a test case
******************

A click on the "+ New Test Case" button will open the test case creation page.

.. figure:: img/test_case/New_Test_Case_Button.png
  :name: New_Test_Case_Button
  :alt: New_Test_Case_Button
  :align: center

  "+ New Test Case" Button

.. figure:: img/test_case/test_case_creation_page.png
  :alt: Test case creation page
  :align: center

  Test Case Creation Page

The test case creation page:
  #. The desired test case name is entered here.
  #. The desired test case description is entered here.
  #. The test case preconditions are entered here.
     Test case preconditions are the conditions that should be met before executing the test steps.
     They can be added by using the "Add" button.
  #. The test steps are added here.
  #. Here the applicability is shown.
     Versions and Variants can be managed and selected here.
     For more information visit the :doc:`Applicability <../applicability>` page.
  #. An estimated test time can be entered here.
  #. Labels can be selected here.
  #. This button cancels the test case creation.
  #. This button saves the test case.

Adding test steps
=================

Clicking on the "Add Test Step" button will open a new dialog:

.. figure:: img/test_case/test_step_creation_dialog.png
  :alt: Test step creation dialog
  :align: center

  Test Step Creation Dialog

The test step creation dialog
  #. The task of the test step is entered here.
  #. The expected result is entered here.
  #. This button adds the test step to the test case.
  #. This button closes the test step dialog.

***********************
Detailed test case view
***********************

.. figure:: img/test_case/test_case_detailed_view.png
  :alt: Detailed test case view
  :align: center

  Detailed Test Case View

The detailed test case view:
  #. This button returns the user to the test cases main page.
  #. This button is used to delete the test case.
  #. This button is used to duplicate the test case.
  #. This button will open the history of the test case.
  #. This button is used to edit the test case.
  #. This button is used to assign members to the test case.
  #. This button starts the test case execution.
  #. The test case description is shown here.
  #. The test case preconditions are shown here.
  #. The test steps are shown here. The expected result can be viewed by clicking on the test step.
  #. Comments are shown here.
  #. New comments can be created here.
  #. The applicability is shown here.
  #. The estimated test duration is shown here.
  #. The number of test steps are shown here.
  #. The labels are shown here. New labels can be added to the test case here.
  #. The assigned members are shown here.

******************
Delete a test case
******************

To delete a test case, click on the "Delete" button.

.. figure:: img/test_case/Delete_Button.png
  :name: Delete_Button
  :alt: Delete_Button
  :align: center

  Delete Button

The deletion needs to be confirmed in the Pop-up by clicking the "Delete" button.

.. figure:: img/test_case/test_case_confirm_delete.png
   :alt: delete confirmation dialog
   :align: center

   Delete Confirmation Dialog

*********************
Duplicate a test case
*********************

Test cases can be duplicated. Duplicated test cases can be used to create similar test cases from existing ones.
to duplicate a test case, click on the "Duplicate" button.

.. figure:: img/test_case/Duplicate_Button.png
  :name: Duplicate_Button
  :alt: Duplicate_Button
  :align: center

  Duplicate Button

Enter the name for the duplicated test case and click on "Duplicate".

.. figure:: img/test_case/test_case_confirm_duplicate.png
   :alt: duplicate confirmation dialog
   :align: center

   Duplicate Confirmation Dialog

**********************
History of a test case
**********************

To view the history of a test case, click on the "History" button.

.. figure:: img/test_case/History_Button.png
  :name: History_Button
  :alt: History_Button
  :align: center

  History Button

The history shows all changes that were made to the test case.

.. figure:: img/test_case/test_case_history_page.png
  :alt: Test case history page
  :align: center

  Test Case History Page

Older versions of the test case can be viewed by clicking on a list entry.
Older versions can be duplicated here.
The current version can also be reverted to an older version here.

.. figure:: img/test_case/test_case_detailed_view_history.png
  :alt: Detailed test case history view
  :align: center

  Detailed Test Case History View

****************
Edit a test case
****************

To edit an existing test case, click on the "Edit" button.

.. figure:: img/test_case/Edit_Button.png
  :name: Edit_Button
  :alt: Edit_Button
  :align: center

  Edit Button

Editing a test case works similar to creating one. for more details read the "Create a Test Case" section.

.. figure:: img/test_case/test_case_editing_page.png
  :alt: Test case editing page
  :align: center

  Test Case Editing Page

******************
Assign a test case
******************

Members of the Project can be assigned to test cases. Assigning members to a test case will create a todo list entry in their TODOs.
To assign a test case to a member, click on the "Assign" button.

.. figure:: img/test_case/Assign_Button.png
  :name: Assign_Button
  :alt: Assign_Button
  :align: center

  Assign Button

Select testers and click on "Assign tester" to assign the test case to the members.

.. figure:: img/test_case/test_case_assign_dialog.png
  :alt: Assign members dialog
  :align: center

  Assign Members Dialog

.. _execute-a-test-case:

*******************
Execute a test case
*******************

To execute a test case, click on the "Start" button.

.. figure:: img/test_case/Start_Button.png
  :name: Start_Button
  :alt: Start_Button
  :align: center

  Start Button

This will open a test case execution page.

.. figure:: img/test_case/test_case_execution_start_page.png
	:alt: Test case execution start page
	:align: center

	Test Case Execution Start Page

The start page:
  #. The met preconditions can be selected here. There are no preconditions in this example, so nothing has to be selected here.
  #. The version can be selected here.
  #. The variant can be selected here.
  #. Proceed to the first test step by clicking on this button.
  #. Anonymous execution can be activated here.
  #. The estimated test duration is shown here.
  #. The number of test steps is shown here.
  #. The test case labels are shown here.

.. figure:: img/test_case/execute_test_case.png
	:alt: Execute test case
	:align: center

	Example Execution Of A Test Case

The first test step:
  #. The test step task is described here.
  #. The assessment can be selected here.
  #. The expected result is shown here.
  #. The actual result can be entered here.
  #. Notes can be entered here.
  #. The test case execution can be aborted here.
  #. This button is not fully implemented yet.
  #. The execution timer is shown here.
  #. The execution timer can paused and unpaused here.
  #. This button is used to proceed to the next test step.

For all further steps, proceed as in the first step.

When all test steps are completed, the summary page is shown.

.. figure:: img/test_case/execute_test_case_summary.png
	:alt:	Example Execution Summary
	:align: center

	Example Execution Summary

The summary page:
	#. A list of all test step results is shown here. The list items can be expanded to show the notes and actual result of a test step.
	#. The assessment of the test case can be selected here.
	#. Any comments and important notes should be written here.
