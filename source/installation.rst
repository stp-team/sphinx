############
Installation
############

.. |br| raw:: html

   <br/>

.. danger::

  SystemTestPortal is still under development and may include potential security issues. It should not be used for running open to the public.

**************************
Installation from Binaries
**************************

.. warning::

	Due to a temporary outage of our CI pipeline the regular stable and nightly builds are outdated at the moment. In the meantime, please use our `tuebix builds <ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/tuebix/binaries/>`__ instead. The installation is the same as for our regular binary builds.

Our FTP server provides binaries for Windows and Linux.

1. Stable binary releases can be found `here <ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/releases>`__ |br|
   Nightly unstable releases can be found `here <ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/nightlies>`__ |br|

   If you just want the latest version you can also go directly to:

   You can download the latest stable release `here <ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/releases/latest>`__ |br|
   The Latest nightly unstable release can be found `here <ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/nightlies/latest>`__ |br|

2. Download the correct archive file

   * Stable releases: ``SystemTestPortal-<os>-<arch>_<version>.<zip|tar.gz>`` where ``<os>`` is your operating system (e.g. windows or linux) and ``<arch>`` is your architecture (If you don’t know your architecture you probably have amd64).
   * Experimental releases: ``SystemTestPortal-<os>-<arch>_<version>.<zip|tar.gz>`` where ``<os>`` is your operating system (e.g. windows or linux), ``<arch>`` is your architecture (If you don’t know your architecture you probably have amd64).

3. Verify the signature of the STP binary:

   * Save following public key to a text file called ``stp-public.key``

      .. code:: 
        
        -----BEGIN PGP PUBLIC KEY BLOCK-----

        mQINBFsjpuIBEADQzSrAecRpDcNmEbrI/oMKfJyWAloq/slDNduvvWRi3qDRXmF8
        lEDRasSZsz1wdqZNDOAcYVHeFkD0berqatxPyGFVoDBICpNWGEFKuEOCdCtdI50J
        YlCe90Bn1WeY5A/1NK1ojhfDOIdPS/X9ArdccVkO9dZDI1XYtfRUR+rghNWELwBs
        7ZIX3FW0yhZYoujesIJprD/dYshM5xcNiQ/O07rJYap0zJ0WIYgg3aFu6dQ4TLcb
        PMRFCS/VICyWuS5cbrZLgsAqkQVx0VKjjcHzsaG/YEoJHKLpwY3xmZxIsFljXblS
        97vL9WYnzGdvuiU46B4Xgt3kPT7GGtebwfN0jY/gHMpZuGXDIMopfX0lK4GhWdx1
        qYeTrhPf9Nvgw6z/g+iySlRCADY6zcaRgGI3tAwzmekuPib9AjEtSWSVtwOAdOBR
        rC5VqV1z+ewuKmMadhpvKaeO1FWrJgWcUcanmMTh40Fqw0N7YHvDIV5zdHkR0Ni2
        /2E+Gs+qXgTVOtkq//DxjW6gYrVKZ5yU3OQsUPE0OqXIHPjkk6TXYn1/0/ANCKr1
        jTvZ+bZif2hK69w95WE7tiSoLAWW4FG4zax61Yiymcm5JWPOSVjrvkr9ddw0VoUM
        AheeCdX/LdVcYVJTygn9J70CgVtSkxUtwGPiGH9v48+KIy695OATRPDQSQARAQAB
        tCdTeXN0ZW10ZXN0cG9ydGFsIChzeXN0ZW10ZXN0cG9ydGFsLm9yZymJAjgEEwEC
        ACIFAlsjpuICGwMGCwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEHB8SXxxKepc
        MuAP/1alcF7qThGtlvQTE22RW/4Oy8TJe+ud6LPnNeW/IEn0hcq7NGrCax9yYUMr
        zOYbdcvrLKcN+BCwp51JEQUyRsmjzCb3ZOXXYzea6fB0ELP3dGHbLL4u0EB7z+Pl
        NmKZc0hNzPo143CgceP/wdAd6EvyssiXhvixNyIdXruANBT2T7Kk+i+SWXQJLzhL
        QT9fw4MMj0ufVwJ9ehVkcRK7CDBAt3hL8xcM0nrOkbW8nLxNwk+LTkM01LG7Yzj6
        Fn7CoFop/LH7dYNun5WRhE/eYKcgESab7+FgjZfF7P2oO15BdNeiy1zHpkLAF+z3
        6k7RYnSBSRjflaiQfXId32FZ8RRJI22tevdg7Bwor5E2bLl//4p2zLm1UB4yZCN+
        xowRSklbznJavFcNy/3XVLb+k/romCchL5r9LL3dD9lLEWZaYUVk8ebrbvSCoXZy
        KIFJp5NLUF4nubuw1MojvtFKvMTxjZ5mYyA71Ar+J9dlrGiLM6VmESbE/+SjPFDW
        7oF0+quHbO2Q47ehhnCwCuzlGAjgw3xQYexe5xn32CdgKSs3OwPz/UFbUSTnJeq4
        Rlt9moereVbXxbNmQEN8Zs9svg5hj0nyJi5rihv0hyxfCO9AnIu/uv1iEJV0qArr
        FHa6apBo+lQgHVgyCqxsGkDgBsLu74SsDACzuJGRoWtZGHXF
        =z6ve
        -----END PGP PUBLIC KEY BLOCK-----

   * Import the public GPG key with: ``gpg --import stp-public.key``
   * Verify the signature and binary you just downloaded: 

     .. code::

        gpg --verify ./SystemTestPortal-<os>-<arch>_<version>.<zip|tar.gz>.sig ./SystemTestPortal-<os>-<arch>_<version>.<zip|tar>.gz

4. Extract the downloaded archive file

5. Run the stp(stp.exe on windows) executable in the extracted directory

Open a web browser and visit http://localhost:8080 to access the application.

***********************
Installation via Docker
***********************

The easiest way to get an STP up and running is by using our Docker Image.

We offer a docker image via `dockerhub <https://hub.docker.com/r/systemtestportal/systemtestportal/>`__
or our `registry on gitlab <https://gitlab.com/stp-team/systemtestportal-webapp/container_registry>`__.

You can simple pull the latest image using:

.. code-block:: bash
 
    docker pull systemtestportal/systemtestportal


To pull a specific tag you can run for example:

.. code-block:: bash

    docker pull systemtestportal/systemtestportal:v1-4-0


Just run the following command to start the container:

.. code-block:: bash

    docker run -p 8080:8080 systemtestportal/systemtestportal:latest

To keep the any actions performed and have persistent data, utilize docker's volume mounting:

.. code-block:: bash

    docker run -dit --rm --mount type=volume,src=stp1,dst=/usr/share/stp/data/ -p 8080:8080 systemtestportal/systemtestportal:latest

You can pull the most current image of our master branch from our
`registry on gitlab <https://gitlab.com/stp-team/systemtestportal-webapp/container_registry>`__

.. code-block:: bash

    docker pull registry.gitlab.com/stp-team/systemtestportal-webapp/signed:master


*****************************************
Installation using our Debian Update Site
*****************************************

.. warning::

  Debian packages are currently outdated.

We also host an apt package repo containing packages of the current release and development builds. The package depends
on systemd and will add a service definition that runs STP.

For your first installation you will need to configure the apt repository:

1. To access the repository you need to tell apt about the
   `key file <http://www.systemtestportal.org/keys/st141539@stud.uni-stuttgart.de.key>`__.
   To download it from the command line, use

   .. code-block:: bash

      wget http://www.systemtestportal.org/keys/st141539@stud.uni-stuttgart.de.key


    This will create a file `st141539@stud.uni-stuttgart.de.key` in your current directory.

2. The following commands will require root access. To become `root`, login as root or run

  .. code-block:: bash

    sudo -s


3. To tell apt about the key file use

  .. code-block:: bash

    apt-key add st141539@stud.uni-stuttgart.de.key


4. Configure a new package source. Run one of the following commands:

 - for stable releases:

   .. code-block:: bash

      echo deb ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/debian stable main > /etc/apt/sources.list.d/systemtestportal.list


 - for experimental releases:

   .. code-block:: bash

      echo deb ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/debian experimental main > /etc/apt/sources.list.d/systemtestportal.list


5. Logout of the root account. If you used `sudo` before, you can just run

  .. code-block:: bash

    exit


Now you are ready to install SystemTestPortal from the package site.

1. The following commands will require root access. To become `root`, login as root or run

  .. code-block:: bash

    sudo -s


2. Run

  .. code-block:: bash

    apt update
    apt install systemtestportal


    as root to read the new package lists and install the package.

3. To enable the service so it is automatically run at startup, run

  .. code-block:: bash

    systemctl enable stp.service


4. You will need to start the service to run for the first time

  .. code-block:: bash

    systemctl start stp.service

5. Logout of the root account with `exit`
6. Open a web browser and visit `http://localhost:8080`(`http://localhost` for versions < 0.7) to access the application.

************************
Installation from Source
************************

You can install the current development version from source. To do so, you will need a working Go installation with a configured
`GOPATH` (At least Golang 1.8). You will additionally need to have the GNU Compiler Collection (gcc) installed to build with sqlite support.
Windows users can get gcc from http://tdm-gcc.tdragon.net/

#. Run

    .. code-block:: bash

      go get gitlab.com/stp-team/systemtestportal-webapp/cmd/stp

    to download and install STP with sqlite support.

#. The installed package can be found under `$GOPATH/bin/stp`
#. Optionally, you can move the asset directories, `migrations`, `templates`, `static` and `data` to another directory


#. Run `stp`


    .. code-block:: bash

      stp


    to start up the server. If you moved the asset directories elsewhere, use the `--basepath` argument:


    .. code-block:: bash

      stp --basepath=/directory/where/you/put/the/assets

#. Open a web browser and visit `http://localhost:8080` to access the application.
#. The default user's login info is:

    * Username: admin
    * Password: admin
    
*******
Hosting
*******

The config.ini file can be used to configure headers for hosting. Available configuration options are: 

    * sslredirect: true or false, only allow HTTPS requests if true.
    * ssltemporaryredirect: true or false, return 302 while redirecting if true, default is 301.
    * stseconds: long, max-age of STS header. Default is 0.
    * stspreload: true or false, appends the preload flag to the STS header, default is false.
    * framedeny: true or false, adds 'DENY' X-Frame-Options header, default is false.
    * contenttypenosniff: true or false, adds 'nosniff' X-Frame-Options header, default is false.
    * browserxssfilter: true or false, adds the '1;mode-block' X-XSS-Protection header, default is false.
    * referrerpolicy: string, sets the value as Referrer-Policy header, default is empty. Formatting should be within ``, e.g. `same-origin`.
    * featurepolicy: string, sets the value as Feature-Policy header, default is empty. Formatting should be within ``, e.g. `vibrate 'none';`
    * csp: string, sets the value as Content-Security-Policy header, default is empty. Formatting should be within ``, e.g. `default-src *`