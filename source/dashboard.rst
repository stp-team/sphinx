#########
Dashboard
#########

The dashboard is an overview of the latest results of the execution of test cases.
It can be viewed by clicking on the "Dashboard" tab inside of a project.

.. figure:: img/dashboard/Dashboard.png
  :name: Dashboard
  :alt: Dashboard
  :align: center

  Example Dashboard

The Dashboard:
  #. The viewed variant can be changed here.
  #. The toggle can be used to switch between test cases and test sequences.
  #. Test cases are shown in this column. A test case can be viewed by clicking on the corresponding entry inside the column.
  #. All subsequent columns show the test results of each version. Clicking on an icon will open the protocol of this test.

.. |notExecuted| image:: img/dashboard/notExecuted.png
  :alt: Test not executed

.. |notAssessed| image:: img/dashboard/notAssessed.png
  :alt: Test result not assessed

.. |passedWithComment| image:: img/dashboard/passedWithComment.png
  :alt: Test passed with comment

.. |passed| image:: img/dashboard/passed.png
  :alt: Test passed

.. |failed| image:: img/dashboard/failed.png
  :alt: Test failed


+---------------------+-----------------------------------------+
| Symbol              | Explanation                             |
+=====================+=========================================+
| |notExecuted|       | This test case has not been executed    |
+---------------------+-----------------------------------------+
| |notAssessed|       | No test result has been assessed        |
+---------------------+-----------------------------------------+
| |passedWithComment| | The test has passed, but with a comment |
+---------------------+-----------------------------------------+
| |passed|            | The test has passed                     |
+---------------------+-----------------------------------------+
| |failed|            | The test has failed                     |
+---------------------+-----------------------------------------+
