######
Labels
######

Labels are a feature to tag test cases and test sequence.
Test cases and test sequences can be filtered by the labels they use.

***************
Managing labels
***************

To open the "Manage Labels" dialog, click on the "Test Cases" button in the navigation bar.
Then click on the wrench icon next to "Labels".

.. figure:: img/labels/Label_Button.png
  :name: Label_Button
  :alt: Label_Button
  :align: center

  This button opens the "Manage Labels" dialog

Labels can be added, edited and deleted in the "Manage Labels" dialog.

.. figure:: img/labels/Dialog_Label.png
  :name: Dialog_Label
	:alt: Dialog_Label
  :align: center

  "Manage Labels" dialog

Adding labels
=============

To add a new label click on the "New Label" button on the left.
This will reset the "Add Label" dialog.

.. figure:: img/labels/Add_Label.png
  :name: Add_Label
	:alt: Add_Label
  :align: center

  Adding a label

The Adding dialog:
  #. The desired label name is entered here.
  #. The desired label description is entered here.
  #. The desired label color can be selected here.
  #. A preview of how the label will look like is shown here.
  #. This button adds the label to the label list and saves it.

Editing labels
==============

Selecting a label in the label list opens an editing dialog that looks similar to the adding dialog.
The label name, description and color can be changed here and a label deletion can be started here.
To apply the changes click on the "Save" button.

.. figure:: img/labels/Edit_Label.png
  :name: Edit_Label
	:alt: Edit_Label
  :align: center

  Editing a label


Deleting labels
===============

To delete a label, first open the label editing dialog and click on the "Delete" button.
This will open a confirmation dialog.
Clicking on the "Delete" button confirms the deletion.

.. figure:: img/labels/Confirm_Delete_Label.png
  :name: Confirm_Delete_Label
  :alt: Confirm_Delete_Label
  :align: center

  Confirmation dialog

***************
Label Filtering
***************

To filter the test case list with labels click on the "Test Cases" button in the navigation bar.
Then click on the labels under "Labels" to filter the list.

.. figure:: img/labels/Filter_Label.png
  :name: Filter_Label
  :alt: Filter_Label
  :align: center

  The clickable filter labels


To remove labels from the filter click on the "x" of the label.

.. figure:: img/labels/Remove_Filter_Label.png
  :name: Remove_Filter_Label
  :alt: Remove_Filter_Label
  :align: center

  This button removes the label from the filter list
