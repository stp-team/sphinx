#########
Main Page
#########

********
Projects
********

The "Projects" page lists all projects in this instance of STP.
Clicking on one of them will open the projects overview tab.

Logged in users can create new projects using the "+ New Project" button.

.. figure:: img/explore/projects.png
	 :alt: Projects overview
	 :align: center

	 An overview of all projects

******
Groups
******

The "Groups" page lists all groups in this instance of STP.
Groups can be created by clicking on the "+ New Group" button.

.. figure:: img/explore/groups.png
	 :alt: Groups overview
	 :align: center

	 An overview of all groups

*****
Users
*****

The "Users" page lists all users using this instance of STP.
More functionality will be added later.

.. figure:: img/explore/users.png
	 :alt: Users overview
	 :align: center

	 An overview of all users
