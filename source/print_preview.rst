#############
Print Preview
#############

There are print previews of test cases, test sequences and protocols.
The intention of this preview is to have a printable list of all test cases or test sequences for use in a meeting for example.
To view a print preview click on "Test Cases", "Test Sequences" or "Protcols" in the navigation bar.
Then click on the print icon in the upper right corner.

.. figure:: img/print_preview/Print_Button.png
  :name: Print_Button
  :alt: Print_Button
  :align: center

  The position of the print preview button

To print a document, click on the print icon in the print preview.
