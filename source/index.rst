#######
Welcome
#######

This is the documentation of the Systemtestportal web app.

.. danger::

  SystemTestPortal is still under development and may include potential security issues. It should not be used for running open to the public.


Besides this documentation, most pages of the Systemtestportal have help buttons.
These help buttons will show a quick reference to the pages functionality and the functions of the buttons.

.. figure:: img/index/help_button.png
  :name: Help Buttons
  :alt: Help Buttons
  :align: center

  The position of the help button

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   explore
   add_user
   groups
   dashboard
   test_cases
   applicability
   test_sequences
   todos
   labels
   protocol
   members
   settings
   print_preview
   imprint
   privacy_notice

.. todolist::
