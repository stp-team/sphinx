#########
Protocols
#########
The Protocols tab gives a list of already executed test cases and test sequences.
Test protocols can be viewed by clicking on the protocols tab inside a project.

.. figure:: img/protocol/protocol_start_page.png
  :alt: Protocol start page
  :align: center

  Protocol Start Page

The protocol start page:
  #. The shown protocols can be toggled between test cases and test sequences.
  #. The shown protocols can be filtered by test cases / test sequences here.
  #. Here the result, the system version of the test case, the system variant of the test case, the tester and the test date are shown.
  #. Clicking on a protocol will show a detailed view of the selected protocol.
  #. Protocols can be further filtered by result, system version and system variant here.


Clicking on a protocol directs the user to a more detailed view of the protocol.

.. figure:: img/protocol/detailed_example_protocol_page.png
  :alt: Example test case protocol page
  :align: center

  Example Test Case Protocol Page

The test case protocol page:
  #. This button can be used to export the test case protocol to PDF.
  #. The test case description is shown here.
  #. The test case preconditions are shown here.
  #. The test step results are shown here. Clicking on the arrow will show the comment.
  #. The execution date, the tester, the system version, the system variant and the test case version are all shown here.


.. figure:: img/protocol/detailed_example_protocol_sequence.png
  :alt: Example test sequence protocol page
  :align: center

  Example Test Sequence Protocol Page

The test sequence protocol page:
  #. This button can be used to export the test case protocol to PDF.
  #. The test sequence description is shown here.
  #. The test sequence preconditions are shown here.
  #. The test case results are shown here.
  #. The execution date, the tester, the system version, the system variant and the test sequence version are all shown here.
