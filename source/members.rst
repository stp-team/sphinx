#######
Members
#######

.. figure:: img/members/Member_Page.png
  :name: Member_Page
  :alt: Member_Page
  :align: center

  Example Member Page

***************
Adding a member
***************

Members can be added to a project by clicking on the "+Add member" button.

.. figure:: img/members/member_adding_button.png
  :name: member adding button
  :alt: member adding dialog
  :align: center

  Member Adding Button

This will open a dialog where members and their roles can be selected.
Clicking on the "Add member" button will add the selected members to the project.

.. figure:: img/members/Member_Adding_Dialog.png
  :name: Member_Adding_Dialog
  :alt: Member_Adding_Dialog
  :align: center

  Example Member Adding Dialog

*****************
Removing a member
*****************

Members can be removed from a project by clicking on the "Remove member" button.

.. figure:: img/members/member_removing_button.png
  :name: member removing button
  :alt: member removing bialog
  :align: center

  Member Removing Button

This will open a dialog where members can be selected.
Clicking on the "Remove member" button will remove the selected members from the project.

.. figure:: img/members/Member_Removing_Dialog.png
  :name: Member_Removing_Dialog
  :alt: Member_Removing_Dialog
  :align: center

  Example Member Removing Dialog
