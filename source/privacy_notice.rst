##############
Privacy Notice
##############

*********************************************************
Data protection information / data protection declaration
*********************************************************

SystemTestPortal is a research project of the `Institute of Software Technology <http://www.iste.uni-stuttgart.de/se.html/>`__ at the `University of Stuttgart <http://www.uni-stuttgart.de>`__.

***********************************************
1. Responsible body under data protection laws:
***********************************************

..::

    University of Stuttgart
    Keplerstrasse 7
    70174 Stuttgart
    Germany

    Telephone:  +49 (0)711/ 685-0
    Email: poststelle@uni-stuttgart.de

**************************
2. Data protection officer
**************************

..::

    University of Stuttgart
    Data protection officer
    Breitscheidstr. 2
    70174 Stuttgart
    Germany

    Tel: +49 711 685-83687
    Fax: +49 711 685-83688
    E-Mail: datenschutz@uni-stuttgart.de

**********************
3. Introductory notice
**********************

This site is operated by `Zentrale Dienste Informatik <http://www.zdi.uni-stuttgart.de/>`__ for the `Institute of Software Technology <http://www.iste.uni-stuttgart.de/se.html>`__ at the `University of Stuttgart <http://www.uni-stuttgart.de>`__.

********************************************************************************************
4. Unless otherwise stated on the respective websites, personal data is gathered as follows:
********************************************************************************************

4.1. Provision of the website and creation of log files
=======================================================

4.1.1 Description and categories of data
----------------------------------------

Should you access this or other websites, you transfer data to our web server via your browser. The following data is temporarily recorded in a log file whilst a connection is established:

- IP address of the accessing computer
- Date and time of the access
- Name, URL and transferred data quantity of the accessed file
- Access status (requested file transferred, not found etc.)
- Browser type and operating system (if transferred by the requesting web browser)
- Website from which the access took place (if transferred by the requesting web browser)

The processing of the data in this log file takes place as follows:

- The log entries are continually automatically evaluated in order to recognize attacks against the web server and in order to be able to respond accordingly.
- In individual cases, i.e. in case of reported disruptions, errors and security breaches, a manual analysis takes place.

In addition, when accessing websites, the date and time stamp, IP address + port (source), IP address + port (target) and the package size are recorded in the active network components of the University of Stuttgart.


4.1.2 Purpose
-------------

The temporary storage of the IP address by the system is necessary in order to deliver the website to the computer of the user. For this purpose, the IP address of the user must remain saved for the duration of the session.

Saving in a log file takes place in order to ensure the functional capability of the website. In addition, the data enables us to optimize the website and to ensure the security of our IT systems. IP addresses contained in the log entries are not combined with other data inventories, unless there are concrete indicators of a disruption to the correct operation.

The recording of active network components also enables us to ensure the security of the IT systems.

These purposes also represent our legitimate interest in the data processing in accordance with Article 6 Paragraph 1 Letter f) GDPR.


4.1.3 Legal basis
-----------------

The legal basis for the temporary storage of the data and log files is Article 6 Paragraph 1 Letter f) GDPR.


4.1.4 Recipients
----------------

Should criminal investigations be initiated due to attacks against our IT systems, the data named under 4.1.1 and log files can be passed on to state investigative bodies (for example the police, criminal prosecution authorities).

The same applies if relevant authorities and/or courts make inquiries of the University and we are obliged to respond to these.


4.1.5 Storage duration
----------------------

The data will be deleted once it is no longer necessary for the purpose for which it was gathered. In case of the recording of data for provision of the website, this is the case when the respective session has come to an end.

The storage of the data in log files is anonymized after seven days. This takes place by means of shortening the IP addresses.


4.1.6 Consequences of non-provision, right of objection and correction
----------------------------------------------------------------------

The recording of data for provision of the website and the storage of data in log files is absolutely necessary in order to operate the website. Users who do not wish for their data to be processed as described can contact the University via alternative channels (by telephone, in written form, in person) in order to receive corresponding information or carry out actions.


4.2. Use of cookies
===================


4.2.1. Description and categories of data
-----------------------------------------

Our website uses cookies. Cookies are text files which are saved in the Internet browser or by the web browser on the computer system of the user. Should a user access a website, a cookie can be saved in the operating system of the user. This cookie contains a character sequence, which enables a clear identification of the browser next time the website is accessed.


4.2.2.Purpose
-------------

Certain functions of our website cannot be provided without the use of cookies. For these, it is necessary for the browser to be recognized again after a change of page.

The user data which is gathered by the technically necessary cookies is not used in order to create user profiles.

These purposes also represent our legitimate interest in the processing of the personal data in accordance with Article 6 Paragraph 1 Letter f) GDPR.

4.2.3. Legal basis
------------------

The legal basis for the processing of personal data using cookies is Article 6 Paragraph 1 Letter f) GDPR.


4.2.4. Recipients
-----------------

The only recipient of the information contained in the cookies is the entitled web server, i.e. the web server of the University which sets the cookie.


4.2.5. Storage duration
-----------------------

The duration of the saving of our cookies is as follows:

Session cookies are automatically deleted from your computer when you close your browser.

As the cookies are saved on your end device, you also have the option of deleting these earlier. You can find out more about this below.


4.2.6. Consequences of non-provision, right of objection and correction
-----------------------------------------------------------------------

Cookies are saved on the computer of the user and transferred from this to our site. Therefore, you as the user also have full control of the use of cookies, regardless of the saving periods listed above. By altering the settings in your web browser, you can deactivate or restrict the transfer of cookies. Cookies which have already been saved can be deleted at any time. This can also take place automatically. Should cookies be deactivated for our website, it may be the case that not all of the functions of the website can be used in full.

**************
5. Your rights
**************

- You have the right to receive information from the university concerning the data saved in relation to your person and/or to have incorrectly saved data corrected.
- In addition, you have the right to deletion or to have the processing restricted or to object to the processing.

For this purpose, please contact the data protection officer of the University of Stuttgart:
datenschutz@uni-stuttgart.de

- You have the right to complain to the supervisory authority, should you be of the opinion that the processing of the personal data relating to you breaches legal regulations.

The competent supervisory authority is the State Data Protection and Freedom of Information Officer of Baden-Württemberg  - `Landesbeauftragte für den Datenschutz und die Informationsfreiheit Baden-Württemberg <https://www.baden-wuerttemberg.datenschutz.de/>`__.
