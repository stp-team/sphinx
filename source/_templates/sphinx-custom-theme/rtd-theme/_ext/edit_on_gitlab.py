"""
Sphinx extension to add ReadTheDocs-style "Edit on GitHub" links to the
sidebar.
Loosely based on https://github.com/astropy/astropy/pull/347
"""

import os
import warnings


__licence__ = 'BSD (3 clause)'


def get_gitlab_url(app, view, path):
    return '{baseurl}/{project}/{view}/{branch}/{base_dir}{path}'.format(
        baseurl=app.config.edit_on_gitlab_baseurl,
        project=app.config.edit_on_gitlab_project,
        view=view,
        branch=app.config.edit_on_gitlab_branch,
        base_dir=app.config.edit_on_gitlab_source_folder,
        path=path)


def html_page_context(app, pagename, templatename, context, doctree):
    if templatename != 'page.html':
        return

    if not app.config.edit_on_gitlab_project:
        warnings.warn("edit_on_gitlab_project not specified")
        return

    #on windows the path contains backslashes resulting in non working urls
    path = os.path.relpath(doctree.get('source'), app.builder.srcdir)

    show_url = get_gitlab_url(app, 'blob', path)
    edit_url = get_gitlab_url(app, 'edit', path)

    context['show_on_gitlab_url'] = show_url
    context['edit_on_gitlab_url'] = edit_url

def setup(app):
    app.add_config_value('edit_on_gitlab_project', '', True)
    app.add_config_value('edit_on_gitlab_source_folder', 'source/', True)
    app.add_config_value('edit_on_gitlab_branch', 'master', True)
    app.add_config_value('edit_on_gitlab_baseurl', 'https://dev.ecadia.com:54243/', True)
    app.connect('html-page-context', html_page_context)
