###############
Getting Started
###############

******************
Register / Sign in
******************

The first step is to sign into Systemtestportal (STP).
Otherwise it is not possible to create or edit any data.

.. figure:: img/quickstart/sign_in.png
	 :alt: Sign in
	 :align: center

	 Start Page

Either ask a registered user to create an account or just use our default user.
(Username: demo, Password: demo)

Click on the "Sign In" button

Insert a valid e-mail address or username and password and click on "Sign In"

.. figure:: img/quickstart/sign_in_screen.png
	 :alt: Sign in screen
	 :align: center

	 Sign In Screen

*******
Execute
*******

Execute a test case
===================

Open the list of test cases by clicking on the "Test Cases" tab.
Now click on one of the existing test cases.

Click on the "Start" button on the test cases overview page to execute the test case.

This will open the test case start page.

.. figure:: img/quickstart/test_case_execution_start_page.png
	:alt: test case execution start page
	:align: center

	Test Case Execution Start Page

The start Page:
	#. The met preconditions can be selected here. There are no preconditions in this example, so nothing has to be selected here.
	#. The version can be selected here.
	#. The variant can be selected here.
	#. Proceed to the first test step by clicking on this button.

.. figure:: img/quickstart/execute_test_case.png
	 :alt: execute test case
	 :align: center

	 Example Execution Of A Test Case

The first test step:
	#. Perform the step task.
	#. Compare the result with the expected result.
	#. Add the actual result
	#. Add notes to the test step.
	#. Rate the test step and choose an adequate result from the list.
	#. Proceed to the next test step by clicking “Next”.

For all further steps, proceed as in the first step.

When all test steps are completed, the summary page is shown.

.. figure:: img/quickstart/execute_test_case_summary.png
	 :alt:	Example Execution Summary
	 :align: center

	 Example Execution Summary

The summary page:
	#. A list of all test step results is shown here. The list items can be expanded to show the notes and actual result of a test step.
	#. The assessment of the test case can be selected here.
	#. Any comments and important notes should be written here.

Execute a test sequence
=======================

Executing test sequences is quite similar to executing test cases.

Start by opening the list of test sequences by clicking on the "Test Sequences" tab.
Then click on one of the existing test sequences.

Click on the "Start" button on the test sequences overview page to execute the test sequence.

Like in test case execution, a start page shows some general information about the test sequence.

.. figure:: img/quickstart/execute_test_sequence.png
	 :alt: Example Test Sequence Execution
	 :align: center

	 Example Test Sequence Execution Start Page

The start page:
	#. Here all met or partially met preconditions can be checked
 	#. The desired version of the test sequence can be selected here.
 	#. The desired variant of the test sequence can be selected here.
 	#. This button is used to proceed to the next test sequence execution step.

After the start page of the test sequence itself, the start page of the first test case appears.
It looks exactly like the start page of a test case,
but a Version and a Variant can not be selected since those are already given by the test sequence.

Execute the test case like before.
At the end of the first test case a summary page will be shown and afterwards the system will continue with the start page of the next test case.

After working through all test cases, the test sequences summary page will be shown with a list of all executed test cases.
Complete the test sequence execution by clicking the Finish button.

.. figure:: img/quickstart/execute_test_sequence_summary.png
	 :alt:	Example Test Sequence Execution Summary
	 :align: center

	 Example Test Sequence Execution Summary

*************
View Protocol
*************

To view protocols of already executed test cases and test sequences click on the "Protocols" tab.
Then select either "Test cases" or "Test Sequences".
Afterwards, select a test case or test sequence from the dropdown list.
Now a list of all protocols for the selected case/sequence is shown.

.. figure:: img/quickstart/protocols.png
	 :alt: Protocol Page
	 :align: center

	 Protocol Page

Protocols can be filtered by test result, system variant and system version using the sidebar on the right.

Click on a test protocol for a detailed view.
In this view, detailed information about the test execution are displayed.

**************
View Dashboard
**************

To get a quick overview of all test results dependent on their given SuT version and variant, you can view the Dashboard.

The Dashboard can be found by clicking on the "Dashboard" tab in your project.
On the left side of the Dashboard is the list of test cases/sequences. The table headers show the different versions of the SuT
and the cells show the latest result of the test case/sequence of this version.

.. figure:: img/quickstart/dashboard_gettingstarted.png
	 :alt: Dashboard
	 :align: center

	 Example Dashboard

The Dashboard:
#. If your SuT has different variants, you can select one via the Dropdown in the upper-left corner.
#. To switch between test cases and test squences, you can press the button in the upper right corner.

******
Create
******

Create a project
================

To create a project click on the "New Project" button on the "Explore" front page.

.. figure:: img/quickstart/create_project_button.png
	 :alt: New Project Button
	 :align: center

	 "New Project" Button On The "Explore" Front Page

Clicking on this button will open a new project creation page

.. figure:: img/quickstart/create_project_dialog.png
	 :alt: Project creation dialog
	 :align: center

	 Project Creation Page

The project creation page:
	#. The desired project name is entered here.
	#. The desired project description is entered here.
	#. A visibility can be selected here.
	#. Pressing this buton creates the new project and redirects the user to the project front page.

Create a test case
==================

To create a test case, click on the “Test Cases” tab inside a project and then click on the “New Test Case” button.

.. figure:: img/quickstart/create_test_case_button.png
	 :alt: "+ New Test Case" button
	 :align: center

	 "+ New Test Case" Button Inside The "Test Cases" Tab

Clicking on this button will open a test case creation dialog.

.. figure:: img/quickstart/create_test_case_dialog.png
	 :alt: Test case creation dialog
	 :align: center

	 Test Case Creation Dialog

The creation page:
	#. The desired test case name is entered here.
	#. The desired test case description is entered here.
	#. The preconditions that should be fulfilled before the test steps are executed are entered here.
	#. The test steps of the test case are created here. Clicking on the "Add Test Step" button will open a test step creation dialog.
	#. The variants and versions that are applicable to the test case are created and selected here (at least one version must be present). They can be created by clicking on the wrench button.
	#. The estimated test time can be entered here.
	#. This button is used for saving the test case.

.. figure:: img/quickstart/add_test_step_dialog.png
	 :alt: Test step creation dialog
	 :align: center

	 The Test Step Creation Dialog

The test step creation dialog:
			#. The test step task is entered here.
			#. The expected result is entered here.
			#. this button adds the test step to the test case.

.. figure:: img/quickstart/create_variant_and_version.png
	 :alt: Variant and version creation dialog
	 :align: center

	 Versions And Variants Creation Dialog

The versions and variants creation dialog:
	#. A version is entered here. Clicking on the "Add" button will add it.
 	#. A version is selected here.
 	#. A version is added to a variant here.
 	#. This button saves added or deleted versions and variants.

Afterwards select a variant and click on a version to add them to the test case.

	.. figure:: img/quickstart/add_variant_and_version.png
	 	:alt: Variant and version add dialog
	 	:align: center

	 	Adding Versions And Variants


Create a test sequence
======================

In some cases it is necessary to execute multiple test cases in a row. Therefore it is wise to create a test sequence.

To create a test sequence, click on the "Test Sequences" tab inside a project and then click on the "+ New Test Sequence" button.

.. figure:: img/quickstart/create_test_sequence_button.png
	 :alt: "+ New Test Sequence" button
	 :align: center

	 "+ New Test Sequence" Button

This will open a test sequence creation dialog:

.. figure:: img/quickstart/create_test_sequence_dialog.png
	 :alt: Test sequence creation dialog
	 :align: center

	 Test Sequence Creation Dialog

The test sequence creation dialog:
	#. The test sequence name is entered here.
	#. The test sequence description is entered here.
	#. The test sequence preconditions are entered here.
	#. The test cases are added here. Testcases can be added by clicking on the "Add Test Case" button.
	#. This button saves the test sequence.

********
Sign out
********

To sign out, click on your username and click "Sign Out" in the emerging dropdown menu.

.. figure:: img/quickstart/sign_out_button.png
	 :alt: "Sign Out" button
	 :align: center

	 "Sign Out" Button
