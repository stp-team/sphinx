######
Groups
######

.. note::

   Groups are not fully implemented yet.

****************
Creating A Group
****************

A group can be created by clicking on the "+ Add New Group" button inside the "Groups" page.
Clicking on the button will open a new group creation page.

.. figure:: img/groups/group_creation.png
   :alt: Group creation page
   :align: center

   Group Creation Page

The Group Creation Page:
  #. The group name is entered here.
  #. The group description is entered here.
  #. The group visibility can be selected here. Currently only "public" is supported.
  #. The members of a group can be selected here. This is currently not supported.
  #. This button is used to create the group.
