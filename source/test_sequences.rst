#############
Test Sequence
#############

Test Sequences are used to test multiple test cases in a row.

************************
Test sequences main page
************************

.. figure:: img/test_sequence/test_sequence_main_page.png
  :alt: Test sequence main page
  :align: center

  Test Sequence Main Page

The test sequence main page:
  #. The test sequence search functionality is not implemented yet.
  #. Clicking on this button will open the test sequence creation page.
  #. A list of all test sequences is shown here. Clicking on an entry will direct the user to a more detailed view of the test case.
  #. Labels are managed here. They can also be used to filter the test sequences. For more informations visit the :doc:`Labels <../labels>` page.

**********************
Create a test sequence
**********************

A click on the "+ New Test Sequence" button will open the test sequence creation page.

.. figure:: img/test_sequence/test_sequence_new_test_sequence_button.png
  :alt: "+ New Test sequence" button
  :align: center

  "+ New Test sequence" Button

.. figure:: img/test_sequence/test_sequence_creation_page.png
  :alt: Test sequence creation page
  :align: center

  Test Sequence Creation Page

The test sequence creation page:
  #. The desired test sequence name is entered here.
  #. The desired test sequence description is entered here.
  #. The test sequence preconditions are entered here.
     Test sequence preconditions are the conditions that should be met before the test cases are executed.
     They can be added by using the "Add" button.
  #. The test cases are added here. They can be added by clicking on the "Add Test Case" button.
  #. The estimated test duration (sum of the estimated test time of all test cases) is shown here.
  #. Labels can be selected here.
  #. This button cancels the test sequence creation.
  #. This button saves the test sequence.

***********************
Detailed test case view
***********************

.. figure:: img/test_sequence/test_sequence_detailed_view.png
  :alt: Detailed test sequence view
  :align: center

  Detailed Test sequence View

The detailed test sequence view:
  #. This button returns the user to the test sequence main page.
  #. This button is used to delete the test sequence.
  #. This button is used to duplicate the test sequence. The duplication functionality is not implemented yet.
  #. This button will open the history of the test sequence.
  #. This button is used to edit the test sequence.
  #. This button is used to assign members to the test sequence.
  #. This button starts the test sequence execution.
  #. The test sequence description is shown here.
  #. The test sequence preconditions are shown here.
  #. The test cases are shown here. A test case can be viewed by clicking on a list entry.
  #. Comments are shown here.
  #. New comments can be created here.
  #. The applicability is shown here. It is automatically determined by the applicability of the test cases.
  #. The estimated test duration is shown here.
  #. The number of test cases are shown here.
  #. The labels are shown here. New labels can be added to the test case here.
  #. The assigned members are shown here.

**********************
Delete a test sequence
**********************

To delete a test sequence, click on the "Delete" button.

.. figure:: img/test_sequence/test_sequence_delete_button.png
  :alt: Delete button
  :align: center

  Delete Button

The deletion needs to be confirmed in the Pop-up by clicking the "Delete" button.

.. figure:: img/test_sequence/test_sequence_confirm_delete.png
   :alt: delete confirmation dialog
   :align: center

   Delete Confirmation Dialog

**************************
History of a test sequence
**************************

To view the history of a test sequence, click on the "History" button.

.. figure:: img/test_sequence/test_sequence_history_button.png
  :alt: History button
  :align: center

  History Button

The history shows all changes that were made to the test sequence.

.. figure:: img/test_sequence/test_sequence_history_page.png
  :alt: Test sequence history page
  :align: center

  Test Sequence History Page

Older versions of the test sequence can be viewed by clicking on a list entry.
The current version can be reverted to an older version here.

.. figure:: img/test_sequence/test_sequence_detailed_view_history.png
  :alt: Detailed test case history view
  :align: center

  Detailed Test Case History View

********************
Edit a test sequence
********************

To edit an existing test case, click on the "Edit" button.

.. figure:: img/test_sequence/test_sequence_edit_button.png
  :alt: Edit button
  :align: center

  Edit Button

Editing a test sequence works similar to creating one. for more details read the "Create a Test Case" section.

.. figure:: img/test_sequence/test_sequence_editing_page.png
  :alt: Test sequence editing page
  :align: center

  Test Case Editing Page

**********************
Assign a test sequence
**********************

Members of the Project can be assigned to test sequence. Assigning members to a test sequence will create a todo list entry in their TODOs.
To assign a test case to a member, click on the "Assign" button.

.. figure:: img/test_sequence/test_sequence_assign_button.png
  :alt: Assign button
  :align: center

  Assign Button

Select testers and click on "Assign tester" to assign the test sequence to the members.

.. figure:: img/test_sequence/test_sequence_assign_dialog.png
  :alt: Assign members dialog
  :align: center

  Assign Members Dialog

***********************
Execute a Test sequence
***********************

To execute a test sequence, click on the "Start" button.

.. figure:: img/test_sequence/test_sequence_start_button.png
  :alt: start Button
  :align: center

  Start Button

This will open a test sequence execution page.

.. figure:: img/test_sequence/test_sequence_execution_start_page.png
	:alt: Rest sequence execution start page
	:align: center

	Test Sequence Execution Start Page

The start page:
  #. The test sequence description is shown here.
  #. The met preconditions can be selected here.
  #. The version can be selected here.
  #. The variant can be selected here.
  #. Proceed to the first test case by clicking on this button.
  #. Anonymous execution can be activated here.
  #. The estimated test duration is shown here.
  #. The number of test cases is shown here.
  #. The test sequence labels are shown here.

The test sequence will execute test cases. The execution of these testcases is explained in :ref:`execute-a-test-case`.
When all test case test steps are completed, the summary page is shown.
The summary page shows a list of all test case results.
Notes for the test cases can be viewed by clicking on the text inside an entry.

.. figure:: img/test_sequence/test_sequence_execution_finish_page.png
	:alt:	Example Execution Summary
	:align: center

	Example Execution Summary
