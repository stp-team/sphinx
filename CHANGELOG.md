## 04/06/2018
### Fixed
- Fixed display issue with wrong file ending
### Removed
- Removed sphinx-autobuild.sh
- Removed navigation_depth from conf.py

## 30/05/2018
### Added
- Added documentation for the dasboard in v1.1
### Changed
- Updated labels to changes from v1.1

## 26/05/2018
### Added
- Added caption to all images
### Changed
- Updated several files
### Fixed
- Fixed issues with displaying some images
### Removed
- Removed most lineblocks, as they were causing issues

## 07/05/2018
### Changed
- Replaced template with the actual documentation