![Build Status](https://gitlab.com/STP-Team/sphinx/badges/master/build.svg)

This project will be used to provide an indepth documentation to the usage of the
[systemtestportal](https://gitlab.com/STP-Team/SystemTestPortal) in the future.

To build the project:
- Install the latest stable version of Python (Currently [Python 3.6.5](https://www.python.org/downloads/release/python-365/))
- Install the latest [python-sphinx](http://www.sphinx-doc.org/en/master/usage/installation.html) using pip (Currently 1.7.5)
- Install the latest [python-sphinx-rtd-theme](https://github.com/rtfd/sphinx_rtd_theme) using pip (Currently v0.4.0)
- Run "make html" in the project folder